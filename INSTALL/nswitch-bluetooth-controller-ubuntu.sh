wget https://deb.nodesource.com/setup_12.x
chmod +x setup_12.x
./setup_12.x
sudo apt install nodejs -y
rm setup_12.x
apt install ffmpeg -y
apt install python3-dbus libhidapi-hidraw0 build-essential libdbus-glib-1-dev libgirepository1.0-dev python3-pip git -y
apt install build-essential libssl-dev libffi-dev python3-dev -y
apt install -y python3-venv -y
apt install python3-setuptools -y
pip3 install wheel
pip3 install dbus-python
# git clone https://gitlab.com/moeiscool/web-nintendo-switch-controller.git
# cd web-nintendo-switch-controller
git clone https://gitlab.com/Shinobi-Systems/joycontrol.git -b amiibo_edits
cd joycontrol
sudo pip3 install .
pip3 install crc8
cd ..
npm install --unsafe-perm
if [ ! -e "./conf.js" ]; then
    sudo cp conf.sample.js conf.js
fi
cc usbreset.c -o usbreset
chmod +x usbreset
