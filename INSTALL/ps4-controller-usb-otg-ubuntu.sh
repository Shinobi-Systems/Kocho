wget https://deb.nodesource.com/setup_12.x
chmod +x setup_12.x
./setup_12.x
sudo apt install nodejs -y
rm setup_12.x
apt install ffmpeg -y
sudo apt-get update
sudo apt-get install build-essential python-dev python-pip git
# git clone https://gitlab.com/moeiscool/web-nintendo-switch-controller.git
# cd web-nintendo-switch-controller
git clone https://gitlab.com/moeiscool/raspberrypijoystick.git
cd raspberrypijoystick
echo "============="
echo "Add rules to /boot/config and /etc/modules?"
echo "Don't do this if you did it already."
echo "(y)es or (N)o"
read -r mysqlagree
if [ "$mysqlagree" = "y" ] || [ "$mysqlagree" = "Y" ]; then
    sudo echo "dtoverlay=dwc2" | sudo tee -a /boot/config.txt
    sudo echo "dwc2" | sudo tee -a /etc/modules
    sudo echo "libcomposite" | sudo tee -a /etc/modules
fi

sudo chmod +x NSGamepad/ns_gamepad_usb
sudo cp NSGamepad/ns_gamepad_usb /usr/bin/

echo "add this to /etc/rc.local"
echo "/usr/bin/ns_gamepad_usb # Raspberry NSGamepad joystick libcomposite configuration"
echo "To edit rc.local :"
echo "sudo nano /etc/rc.local"
cp INSTALL/ps4-controller-usb.sh ./
# sudo python3 gamepad_ns_keyboard_ps4.py
cd ..
npm install --unsafe-perm
if [ ! -e "./conf.js" ]; then
    sudo cp conf.sample.js conf.js
fi
echo "Done! Reboot required."
