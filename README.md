# Kocho

> This just controls and display my game consoles in the web-browser. This is a web-based controller for your Nintendo Switch or Sony Playstation 4. There is no affiliation to either.

> Use ethernet for this setup wherever possible!

**See Demo**
- http://m03.ca
    - 3 Nintendo Switch Consoles
        - One is 24/7 Zelda : Breath of the Wild
    - 1 Playstation 4
        - First game running is Ghost of Tsushima
        - Vancouver, BC, Canada users will have the lowest latency.
- https://www.youtube.com/watch?v=KGuLjNP14CY
    - 640x480, 30fps, 0.2s latency

**This project provides**
- Web-based proxy for controlling Nintendo Switch with `JoyControl` or `RaspberryPi-Joystick`
    - Bluetooth Adapter Required for **Method 1**
    - OTG Capable Server Required for **Method 2**
- Video Stream to Browser
    - Uses v4l2 with `/dev/video0`. `/dev/video0` is the path that my capture card appears on.
- Viewer Count
- Streams are 800x600, 25fps
- Nickname System
- Live Chat (last 10 messages on page load)

Licensed under GPLv3

## Requirements

> Use the installer to get the software requirements.
- Software
    - JoyControl (Install Method 1) : https://github.com/Poohl/joycontrol fork of https://github.com/mart1nro/joycontrol
        - This method requires a Bluetooth Adapter (mart1nro recommends USB Adapters, not integrated)
    - RaspberryPi-Joystick (Install Method 2) : https://gitlab.com/moeiscool/raspberrypijoystick fork of https://github.com/milador/RaspberryPi-Joystick
        - This method requires a MayFlash Magic-S Pro USB Adapter.
    - FFmpeg (default one from Ubuntu Repos works)
    - Node.js v12
    - Python 3
    - Ubuntu 18.04 (or higher) : Not needed probably but it will be easier to install JoyControl and this wrapper.
- Hardware
    - Capture Card for Video Stream
    - Bluetooth Adapter for Nintendo Switch Control
    - Server for Mediation (I used a Jetson Nano 4GB for 1 capture card and an old Alienware laptop for 3 capture cards)

## How to use

> You must be root to use this for now... or use sudo but you will need to know when to use and when not.

1. Install Ubuntu 18.04 or 20.04.
    - Jetson Nano : Flash your Jetson Nano SD Card with JetPack 4.4 (or higher if it works). Then install dependencies after first boot.
        - `Should work for Raspberry Pi as well.`
        - `Do not attempt to remove the desktop environment. I did this and it caused a latency of 0.7s which made it unplayable. I am unsure how they relate.`
    - Laptop or Desktop : Write an Ubuntu ISO to a USB stick and install the OS from that.

2. There are two ways to install.
    - **Method 1 :** Bluetooth Connection to Nintendo Switch : Requires bluetooth adapter on the streaming device (Jetson Nano or Laptop).
        - Uses a fork of JoyControl by `Poohl` which was originally written by `mart1nro`
    - **Method 2 :** OTG USB from device : like Raspberry Pi 4 USB-C. The device requires an OTG USB port built-in to it.
        - This method emulates a NS Gamepad but it is designed to work with MayFlash Magic-S USB Adapter to control Playstation 4 (PS4)
        - Uses a modified version of RaspberryPi-Joystick by milador
    - Get the Repository.
        ```
        git clone https://gitlab.com/Shinobi-Systems/Kocho.git
        cd Kocho
        ```
    - **Method 1 :** Install to Control Nintendo Switch
        ```
        dos2unix INSTALL/nswitch-bluetooth-controller-ubuntu.sh
        sh INSTALL/nswitch-bluetooth-controller-ubuntu.sh
        ```
    - **Method 2 :** Install to Control Playstation 4
        ```
        dos2unix INSTALL/ps4-controller-usb-otg-ubuntu.sh
        sh INSTALL/ps4-controller-usb-otg-ubuntu.sh
        ```
        Then open `conf.js` and add the following to `line 2`.
        ```
        usbController: true,
        ```

3. The ending portion has different chores for each method.
    - **Method 1 :** Set your Nintendo Switch to the "Change Grip" screen then start the script (see last step). After connecting once : Modify the `conf.js` file to have your Nintendo Switch's Bluetooth Address.
        - If you don't know the Bluetooth Address you can see it appear in the log stream on the game screen web page when it connects the first time.
    - **Method 2 :** For simplicity we'll refer to the server with OTG as RaspberryPi. Before plugging in the RaspberryPi + MayFlash you need to make sure the Playstation 4 is already on for at least 2 seconds (welcome screen, before profile select). Then once the RaspberryPi is powered on wait another 1 minute before running `app.js`. If you do not wait long enough after RaspberryPi boot it will not register any controller actions.

4. Run.

    ```
    node app.js
    ```

5. Open Web Browser at http://IP_OF_SERVER:3001

6. Connect a controller or use keyboard to client with browser open.
    - There is a Keyboard Mapper in the client side, this saves settings to localStorage.

> It may take a few seconds for the controller to become connected when the app.js script is starting.

## Help!

Right now pretty much everything works except for the following functions.
- Wake Nintendo Switch from Bluetooth (in case the Nintendo Switch turns off)
- Rumble Controller on Client side
- Audio on Client side
- Motion Controls
- Touchpad (For PS4, Method 2)

## Daemonize

> Method 2 users remember to see Step 3.

```
npm install pm2 -g
pm2 start app.js
```

Run on startup

> Run the previous commands to daemonize first. Method 2 users cannot use this.

```
pm2 startup
pm2 save
```

**Support Development**
- PayPal : https://paypal.me/ShinobiCCTV
- Subscription like Patreon ($5 USD/month) : https://licenses.shinobi.video/subscribe?planSubscribe=plan_G31AZ9mknNCa6z
- Contribute Code

Copyright 2021 moeiscool (Moe Alam), Shinobi Systems (https://shinobi.systems)
Please contact me for commercial use.
