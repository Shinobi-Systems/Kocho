process.on('uncaughtException', function (err) {
    console.error('uncaughtException',err);
});
const config = require('./conf.js')
console.log(config)
const { createVideoStream } = require('./libs/videoStream.js');
const { createWebServerApplication } = require('./libs/webServer.js');
const {
    http,
    app,
    io,
    videoSocket,
} = createWebServerApplication({
    port: config.webPort,
    videoPort: config.videoStreamPort,
    onClientConnect: (socket) => {
        bindSocketHandlerToPlayers(socket)
        bindSocketChatHandlerToPlayers(socket)
    },
})
const { bindSocketChatHandlerToPlayers } = require('./libs/chattingOverWebsocket.js')(io,config);
const {
    createController,
    bindSocketHandlerToPlayers,
    startAllControllers
} = require('./libs/players.js')(io,config);
//turn video data into mjpeg thn relay to pipe
const ffmpeg = createVideoStream(config.ffmpegBinary || 'ffmpeg',config.videoPath || '/dev/video0',config)
//get video pipe data and send to dedicated websocket
ffmpeg.onImage.on('data',(data) => {
    videoSocket.emitVideo(data)
})

startAllControllers()
