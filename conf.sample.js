module.exports = {
    webPort: 3001,
    nintendoSwitchBluetoothAddress: null, //make null to connect on "Change Grip" screen.
    videoPath: '/dev/video0',
    ffmpegBinary: 'ffmpeg',
    // bluetoothDevices: ["00:00:00:00:00:00"],
}
