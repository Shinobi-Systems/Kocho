const Filter = require('bad-words');
module.exports = (io,config) => {
    const filter = new Filter({ regex: /\*|\.|$/gi });
    const messageLogInMemory = []
    const bindSocketChatHandlerToPlayers = (socket) => {
        let messageTimeout = null
        messageLogInMemory.forEach((oldPost) => {
            socket.emit('chatMessage',oldPost)
        })
        socket.on('chatMessage',(message) => {
            if(messageTimeout)return;
            const cleanedMessage = filter.clean(message.replace(/<\/?[^>]+(>|$)/g, "").trim().substr(0, 255))
            if(!cleanedMessage)return;
            const uuid = socket.uuid && socket.uuid !== 'undefined' ? socket.uuid : socket.id
            const newPost = {
                senderId: socket.uuid,
                senderName: socket.name,
                text: cleanedMessage,
                time: new Date(),
            }
            messageLogInMemory.push(newPost)
            if(messageLogInMemory.length > 10)messageLogInMemory.shift()
            io.emit('chatMessage',newPost)
            messageTimeout = setTimeout(() => {
                messageTimeout = null
            },2000)
        })
    }
    return {
        bindSocketChatHandlerToPlayers: bindSocketChatHandlerToPlayers,
    }
}
