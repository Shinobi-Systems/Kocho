const fs = require('fs');
const spawn = require('child_process').spawn;
const execSync = require('child_process').execSync;
var killProcess = false
process.on('SIGINT', function() {
    killProcess = true
    process.exit();
});
const getAmiibos = async () => {
    try{
        return await fs.promises.readdir(`${process.cwd()}/amiibo/`);
    }catch(err){
        console.log(err)
        return []
    }
}
const getAllBluetoothAdapters = () => {
    const devices = []
    const list = execSync(`hcitool dev`).toString().split('\n')
    list.shift()
    list.forEach((line) => {
        const parts = line.trim().split('\t')
        if(parts[1] && parts[1].indexOf(':') > -1){
            const id = parts[0].trim()
            const address = parts[1].trim()
            devices.push(address)
        }
    })
    return devices
}
const getBluetoothAdapterPath = () => {
    return new Promise((resolve,reject) => {
        const lines = []
        const usbFound = []
        const resetProcess = spawn(`lsusb`,[])
        resetProcess.stdout.on('data',(data) => {
            const line = data.toString().trim()
            lines.push(...line.split('\n'))
        })
        resetProcess.stderr.on('data',(data) => {
            const line = data.toString().trim()
            lines.push(...line.split('\n'))
        })
        resetProcess.on('exit',(data) => {
            lines.forEach((line) => {
                if(line.indexOf('Bluetooth') > -1){
                    const parts = line.split(' ')
                    usbFound.push(`${parts[1]}/${parts[3].replace(':','')}`)
                }
            })
            resolve(usbFound)
        })
    })
}
const restartBluetoothAdapter = (usbPath,controllerProcess) => {
    return new Promise((resolve,reject) => {
        const resetProcess = spawn(process.cwd() + '/usbreset',[`/dev/bus/usb/${usbPath}`])
        resetProcess.stdout.on('data',(data) => {
            const line = data.toString().trim()
            console.log(line)
        })
        resetProcess.stderr.on('close',(data) => {
            setTimeout(() => {
                const resetProcess = spawn(`bash`,[process.cwd() + '/resetUsb.sh'])
                resetProcess.stdout.on('data',(data) => {
                    const line = data.toString().trim()
                    console.log(line)
                })
                resetProcess.stderr.on('close',(data) => {
                    resolve()
                })
            },2000)
        })
    })
}
const createNintendoSwitchController = (options) => {
    console.log(`Starting ${options.isUSB ? 'USB' : 'Bluetooth' } Nintendo Pro Controller`)
    options = options ? options : {}
    const bluetoothDevice = options.bluetoothDevice
    const bluetoothAddress = options.bluetoothAddress
    const controllerParams = options.isUSB ? [
        options.controllerScript || process.cwd() + '/raspberrypijoystick/NSGamepad/Code/gamepad_ns_keyboard_ps4.py',
    ] : [
        options.controllerScript || process.cwd() + '/joycontrol/run_controller_cli.py',
        'PRO_CONTROLLER',
    ]
    if(!options.isUSB && bluetoothDevice){
        controllerParams.push(
            '-d',
            bluetoothDevice
        )
    }
    if(!options.isUSB && bluetoothAddress){
        controllerParams.push(
            '-r',
            bluetoothAddress
        )
    }
    console.log(controllerParams.join(' '))
    // const bluetoothRegister = spawn('bluetoothctl',['power', 'off'])
    // const bluetoothRegister2 = spawn('bluetoothctl',['power', 'on'])
    let controllerProcess = spawn('python3',controllerParams)

    controllerProcess.stdout.on('data',(data) => {
        options.onStdout(data.toString())
    })

    controllerProcess.stderr.on('data',(data) => {
        const string = data.toString();
        if(string.indexOf('Software caused connection abort') > -1){
            controllerProcess.kill()
        }else{
            options.onStderr(string)
        }
    })

    controllerProcess.stderr.on('close',(data) => {
        console.log('killProcess',killProcess)
        options.onExit(killProcess)
    })

    let sendCommand = (text) => {
        // console.log('sendCommand',text + '\n')
        try{
            controllerProcess.stdin.write(text + '\n')
        }catch(err){
            options.writeError(err)
            try{
                controllerProcess.exit()
            }catch(err){
                console.log(err)
            }
        }
    }
    return {
        controllerProcess: controllerProcess,
        sendCommand: sendCommand,
    }
}
module.exports = {
    createNintendoSwitchController: createNintendoSwitchController,
    restartBluetoothAdapter: restartBluetoothAdapter,
    getBluetoothAdapterPath: getBluetoothAdapterPath,
    getAmiibos: getAmiibos,
    getAllBluetoothAdapters: getAllBluetoothAdapters,
}
