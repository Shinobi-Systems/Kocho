const Filter = require('bad-words');
const {
    createVote,
    clearVotes
} = require('./utils.js');
const {
    createNintendoSwitchController,
    restartBluetoothAdapter,
    getBluetoothAdapterPath,
    getAllBluetoothAdapters
} = require('./nintendoSwitchController.js');
const {
    createWebServerApplication,
    getIpAddress
} = require('./webServer.js');
module.exports = (io,config) => {
    const filter = new Filter({ regex: /\*|\.|$/gi });
    const usersConnected = {}
    const playerControllers = {}
    let jumpTimer = null
    const stickReset = (controllerNumber) => {
        controllerNumber = controllerNumber ? controllerNumber : 0
        const playerController = playerControllers[controllerNumber]
        playerController.send(`stick right h 2048`)
        playerController.send(`stick right v 2048`)
        playerController.send(`stick left h 2048`)
        playerController.send(`stick left v 2048`)
    }
    const createController = (controllerNumber) => {
        let { sendCommand } = createNintendoSwitchController({
            isUSB: config.usbController,
            bluetoothDevice: config.bluetoothDevices[controllerNumber],
            bluetoothAddress: config.nintendoSwitchBluetoothAddress,
            onStdout: (string) => {
                // console.log(string)
                if(
                    string !== 'cmd >> ' &&
                    string.indexOf(`StickState was set to`) === -1 &&
                    string.indexOf(`Unexpected stick value`) === -1
                )io.emit('controllerStdout',controllerNumber + ', ' + string)
            },
            onStderr: (string) => {
                io.emit('controllerStderr',controllerNumber + ', ' + string.replace(`root _reply_to_sub_command::244 INFO - received output report - Sub command SubCommand.`,''))
            },
            onExit: (allowKill) => {
                console.log('CRASHED')
                if(!allowKill){
                    recreateControllerOnFail(controllerNumber)
                }else{
                    sendCommand = (text) => {
                        console.log('dropped : ', text)
                    }
                }
            },
            writeError: (err) => {
                sendCommand = (text) => {
                    io.emit('controllerStderr',controllerNumber + ', ' +`Restarting Controller...`)
                }
                recreateControllerOnFail(controllerNumber)
            }
        })
        if(!playerControllers[controllerNumber]){
            playerControllers[controllerNumber] = {
                send: sendCommand,
                lockedBy: null,
                lockTimeout: null,
                restartTimer: null,
                stickResetTimer: null,
                jumpTimer: null
            }
        }else{
            playerControllers[controllerNumber].send = sendCommand
        }
        return sendCommand
    }
    const recreateControllerOnFail = (controllerNumber) => {
        controllerNumber = controllerNumber ? controllerNumber : 0;
        const playerController = playerControllers[controllerNumber]
        clearTimeout(playerController.restartTimer)
        playerController.restartTimer = setTimeout(() => {
            createController(controllerNumber)
            console.log('Restarting Controller ' + controllerNumber);
        },4000)
    }
    const unbindPlayerFromController = (controllerNumber) => {
        controllerNumber = controllerNumber ? controllerNumber : 0;
        const playerController = playerControllers[controllerNumber]
        clearTimeout(playerController.lockTimeout)
        const user = usersConnected[playerController.lockedBy]
        if(user){
            user.isPlaying = false
        }
        playerController.lockedBy = null
        io.emit('playerChange',{
            userId: null,
            controllerNumber: controllerNumber,
        })
        clearInterval(playerController.jumpTimer)
        playerController.jumpTimer = setInterval(() => {
            playerControllers[controllerNumber].send(`hold x`)
            setTimeout(() => {
                playerControllers[controllerNumber].send(`release x`)
            },100)
            stickReset()
        },1000 * 60 * 5)
    }
    const bindPlayerToController = (userId,controllerNumber) => {
        controllerNumber = controllerNumber ? controllerNumber : 0;
        const playerController = playerControllers[controllerNumber]
        clearInterval(playerController.jumpTimer)
        clearTimeout(playerController.stickResetTimer)
        playerController.lockedBy = `${userId}`
        usersConnected[userId].isPlaying = true
        io.emit('playerChange',{
            userId: userId,
            controllerNumber: controllerNumber,
        })
    }
    const resetControllerUnbindTimeout = (controllerNumber) => {
        controllerNumber = controllerNumber ? controllerNumber : 0;
        const playerController = playerControllers[controllerNumber]
        clearTimeout(playerController.lockTimeout)
        playerController.lockTimeout = setTimeout(() => {
            clearTimeout(playerController.stickResetTimer)
            playerController.stickResetTimer = setTimeout(() => {
                stickReset(controllerNumber)
            }, 10000)
            unbindPlayerFromController(controllerNumber)
        }, 10000)
    }
    const giveControl = (controllerNumber,userGetting,userGiving) => {
        controllerNumber = controllerNumber ? controllerNumber : 0;
        const playerController = playerControllers[controllerNumber]
        if(playerController.lockedBy === userGiving && userGetting !== userGiving){
            unbindPlayerFromController(controllerNumber)
            if(usersConnected[userGetting]){
                bindPlayerToController(userGetting,controllerNumber)
                resetControllerUnbindTimeout()
            }else{
                clearTimeout(playerController.lockTimeout)
            }
            clearVotes(`reason`)
        }
    }
    let cleanPlayerCommand = (cmd) => {
        const newList = []
        cmd.forEach((part) => {
            if(part.indexOf('home') === -1){
                newList.push(part)
            }
        })
        return newList.join('&&')
    }
    if(config.allowHomeButton){
        cleanPlayerCommand = (cmd) => {
            return cmd.join('&&')
        }
    }
    const getCurrentPlayers = () => {
        const newList = {}
        Object.keys(playerControllers).forEach((n) => {
            const playerController = playerControllers[n]
            newList[n] = playerController.lockedBy
        })
        return newList
    }
    const bindSocketHandlerToPlayers = (socket) => {
        if(!config.videoOnly){
            const uuid = socket.handshake.query.uuid
            const ipAddress = getIpAddress(socket.request)
            const userObject = {
                ipAddress: ipAddress,
                id: socket.id,
                uuid: uuid,
                joinTime: new Date(),
                isPlaying: false,
            }
            usersConnected[socket.id] = userObject
            socket.uuid = uuid;
            socket.controllerChosen = 0;
            io.emit(`userConnected`,{
                id: userObject.id,
                uuid: uuid,
                joinTime: userObject.joinTime,
                isPlaying: userObject.isPlaying,
            })
            socket.emit(`initiate`,{
                currentPlayers: getCurrentPlayers(),
                users: Object.values(usersConnected),
                amiibos: config.amiiboList || [], //loadedAmiibos
            })
            socket.on('setName',(name) => {
                var userName = filter.clean(name + '')
                socket.name = userName
                usersConnected[socket.id].name = userName
                // userUpdate
                io.emit('userUpdate',{
                    id: socket.id,
                    name: userName
                })
            })
            socket.on('chooseController',(controllerNumber) => {
                socket.controllerChosen = playerControllers[controllerNumber] ? controllerNumber : 0
                socket.emit('controllerChange',controllerNumber)
            })

            socket.on('giveControl',(data) => {
                if(typeof data === 'string'){
                    //legacy first player give control
                    giveControl(0,data,socket.id)
                }else{
                    const userId = data.userId
                    const controllerNumber = data.controllerNumber
                    giveControl(controllerNumber,userId,socket.id)
                }
            })

            //// usb controller reset
            // socket.on('voteControllerReset',() => {
            //     const reason = 'controllerReset'
            //     const voterId = socket.uuid
            //     const maxBase = socket.client.conn.server.clientsCount
            //     createVote({
            //         reason: reason,
            //         voterId: voterId,
            //         maxBase: maxBase,
            //         holdTime: 1000 * 60 * 10,
            //         onVote: (voteQueue,maxRequired) => {
            //             io.emit('vote',{
            //                 reason: reason,
            //                 userId: voterId,
            //                 currentlyVoted: voteQueue,
            //                 maxRequired: maxRequired,
            //             })
            //         },
            //         onPass: async () => {
            //             io.emit('controllerStderr',`Controller Reset Vote Passed!`)
            //             const foundUsbs = await getBluetoothAdapterPath()
            //             await restartBluetoothAdapter(foundUsbs[0])
            //         },
            //     })
            // })

            //// soft-boot vote
            socket.on('voteControllerUnbind',async () => {
                const playerController = playerControllers[socket.controllerChosen]
                if(playerController.lockedBy){
                    const reason = 'controllerUnbind' + socket.controllerChosen
                    const voterId = socket.uuid
                    const maxBase = socket.client.conn.server.clientsCount
                    createVote({
                        reason: reason,
                        voterId: voterId,
                        maxBase: maxBase,
                        onVote: (voteQueue,maxRequired) => {
                            io.emit('vote',{
                                reason: reason,
                                userId: voterId,
                                currentlyVoted: voteQueue,
                                maxRequired: maxRequired,
                            })
                        },
                        onPass: () => {
                            io.emit('controllerStderr',`Controller Unbind Vote Passed!`)
                            unbindPlayerFromController(socket.controllerChosen)
                        },
                    })
                }else{
                    unbindPlayerFromController(socket.controllerChosen)
                }
            })

            //game control input (controller, keyboard)
            socket.on('com',(cmd) => {
                const playerController = playerControllers[socket.controllerChosen]
                if(!playerController.lockedBy){
                    bindPlayerToController(socket.id,socket.controllerChosen)
                    playerController.send(cleanPlayerCommand(cmd))
                    clearVotes(`reason`)
                }else if(playerController.lockedBy === socket.id){
                    playerController.send(cleanPlayerCommand(cmd))
                    resetControllerUnbindTimeout(socket.controllerChosen)
                }else{
                    // send message that another user is playing
                    // wait for them to idle for 10 seconds.
                    socket.emit('lockCom',playerController.lockedBy)
                }
            })

            socket.on('disconnect',() => {
                const playerController = playerControllers[socket.controllerChosen]
                if(playerController.lockedBy === socket.id){
                    unbindPlayerFromController(socket.controllerChosen)
                }
                delete(usersConnected[socket.id])
                io.emit(`userDisconnected`,socket.id)
            })
        }
    }
    const startAllControllers = () => {
        if(!config.videoOnly){
            if(!config.bluetoothDevices)config.bluetoothDevices = config.bluetoothDevice ? [config.bluetoothDevice] : []
            if(config.bluetoothDevices.length === 0){
                config.bluetoothDevices = getAllBluetoothAdapters()
            }
            let connectTimer = 0
            config.bluetoothDevices.forEach((device,controllerNumber) => {
                setTimeout(() => {
                    createController(controllerNumber)
                },connectTimer)
                connectTimer += 20000
            })
        }
    }
    return {
        stickReset: stickReset,
        giveControl: giveControl,
        stickReset: createController,
        createController: createController,
        bindSocketHandlerToPlayers: bindSocketHandlerToPlayers,
        recreateControllerOnFail: recreateControllerOnFail,
        unbindPlayerFromController: unbindPlayerFromController,
        bindPlayerToController: bindPlayerToController,
        resetControllerUnbindTimeout: resetControllerUnbindTimeout,
        getCurrentPlayers: getCurrentPlayers,
        startAllControllers: startAllControllers,
    }
}
