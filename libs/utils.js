const votesInProgress = {}
const votesInProgressTimeouts = {}
const clearVotes = (reason) => {
    votesInProgress[reason] = []
    clearTimeout(votesInProgressTimeouts[reason])
}
const createVote = (options) => {
    const {
        reason,
        voterId,
        maxBase,
        onVote,
        onPass,
        holdTime,
    } = options
    if(votesInProgressTimeouts[reason])return;
    // max votes required is 10 OR the number of players in the room minus the one.
    const maxRequired = maxBase > 10 ? 10 : maxBase - 2 > 0 ? maxBase - 2 : 1
    if(!votesInProgress[reason])votesInProgress[reason] = []
    const voteQueue = votesInProgress[reason]
    if(voteQueue.indexOf(voterId) === -1){
        voteQueue.push(voterId)
        onVote(voteQueue,maxRequired)
    }
    if(voteQueue.length >= maxRequired){
        votesInProgress[reason] = []
        onPass()
        votesInProgressTimeouts[reason] = setTimeout(() => {
            votesInProgressTimeouts[reason] = null
        },holdTime || 1000 * 5)
    }
}
module.exports = {
    createVote: createVote,
    clearVotes: clearVotes,
}
