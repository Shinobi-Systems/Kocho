const spawn = require('child_process').spawn;
const EventEmitter = require('events');
module.exports = {
    createVideoStream: (ffmpegCmd,input,config) => {
        const myEmitter = new EventEmitter();
        const inputPipe = config.isMacOs ? [
            '-f',
            'avfoundation',
            '-framerate',
            '30',
            '-video_size',
            '800x600',
            '-hwaccel',
            'auto',
            '-i',
            '0'
        ] : [
            '-f',
            'v4l2',
            '-framerate',
            '25',
            '-video_size',
            '800x600',
            '-loglevel',
            'warning',
            '-hwaccel',
            'auto',
            '-i',
            input,
        ]
        const mjpegPipe = [
            '-pix_fmt',
            'yuvj444p',
            '-an',
            '-c:v',
            'mjpeg',
            '-q:v',
            '1',
            '-f',
            'image2pipe',
            'pipe:1',
            '-vsync',
            '1',
        ]
        const finalCmd = inputPipe.concat(
            mjpegPipe,
        )
        console.log('Starting Video Stream')
        console.log('ffmpeg ' + finalCmd.join(' '))
        const controllerProcess = spawn(ffmpegCmd || 'ffmpeg',finalCmd)
        let onImageData = (d) => {
            myEmitter.emit('data',Buffer.concat([d]))
        }
        let onImage = onImageData
        var i = 0
        controllerProcess.stderr.on('data',(data) => {
            // const text = data.toString()
            // console.log(text)
            // if(text.indexOf('Past duration') > -1){
            //     onImage = () => {
            //         ++i
            //     }
            //     setTimeout(() => {
            //         console.log(`Dropping ${i} buffers`)
            //         onImage = onImageData
            //         i = 0
            //     },2000)
            // }
        })
        controllerProcess.stdout.on('data',onImage)
        return {
            process: controllerProcess,
            onImage: myEmitter,
        }
    }
}
