const fs = require('fs');
const express = require('express');
const url = require('url');
let numberOfConnectedViewers = 0
module.exports = {
    createWebServerApplication: (options) => {
        options = options ? options : {}
        options.port = options.port || 3001
        var app = express();
        var http = require('http').createServer(app);
        var io = require('socket.io')(http);
        io.engine.ws = new (require('cws').Server)({
            noServer: true,
            perMessageDeflate: false
        })
        const addViewerToCount = (theNumber) => {
            numberOfConnectedViewers += theNumber
            io.emit('viewers',numberOfConnectedViewers)
        }
        io.on('connection', (socket) => {
            var lastPingTime
            var currentMs = 0
            addViewerToCount(1)
            if(options.onClientConnect){
                options.onClientConnect(socket)
            }
            if(options.onClientEvent){
                Object.keys(options.onClientEvent).forEach((key) => {
                    socket.on(key,options.onClientEvent[key])
                })
            }
            var startHeartBeat = setInterval(function(){
                lastPingTime = new Date()
                socket.emit('heartbeat',{})
            },5000)
            socket.on('heartbeat',() => {
                var ms = (new Date()) - lastPingTime;
                socket.emit('ms',ms)
            })
            socket.on('disconnect',() => {
                clearInterval(startHeartBeat)
                addViewerToCount(-1)
            })
        })

        app.set('views', process.cwd() + '/web')

        app.set('view engine','ejs')

        app.use('/', express.static(process.cwd() + '/web'))

        app.get('/music/list', (req, res) => {
            res.setHeader('Content-Type', 'application/json')
            const musicFolder = process.cwd() + '/web/libs/music/'
            fs.readdir(musicFolder,(err,files) => {
                const songsFound = []
                if(files){
                    files.forEach((file) => {
                        if(file.indexOf('.mp3') > -1){
                            const fileParts = file.split(' - ')
                            const songName = fileParts[0]
                            const artistName = fileParts[1].replace('.mp3','')
                            const webPath = `/libs/music/${file}`
                            songsFound.push({
                                song: songName,
                                file: file,
                                artist: artistName,
                                webPath: webPath,
                            })
                        }
                    })
                }
                res.end(JSON.stringify(songsFound,null,3))
            })
        })

        app.get('/', (req, res) => {
            res.render('pages/consolePage.ejs',{
                consoleId: 0,
                gameConsole: {
                    canPlay: 4,
                },
            })
        })

        //plain websocket for video stream
        const WebSocket = require('cws');

        const wss = new WebSocket.Server({
            noServer: true
        })

        wss.on('connection',(client) => {
            var throttleFps = false;
            var fpsCount = 0
            function onMessageThrottled(data) {
                ++fpsCount
                if(fpsCount === 3){
                    fpsCount = 0
                    return
                }
                client.send(data)
            }
            function onMessage(data) {
                client.send(data)
            }
            client.sendData = onMessage
            client.on('message', (message) => {
                if(message === 'throttle'){
                    throttleFps = !throttleFps
                    if(throttleFps){
                        client.sendData = onMessageThrottled
                    }else{
                        client.sendData = onMessage
                    }
                }
            })
        })

        // we use this in app.js
        wss.emitVideo = function(data) {
          wss.clients.forEach((client) => {
               try{
                   client.sendData(data)
               }catch(err){
                   // console.log(err)
               }
          })
        }

        //handle which websocket to use.
        http.on('upgrade', function upgrade(request, socket, head) {
            const pathname = url.parse(request.url).pathname;
            if (pathname === '/videoStream') {
                wss.handleUpgrade(request, socket, head, function done(ws) {
                    wss.emit('connection', ws, request);
                });
            } else if (pathname.indexOf('/socket.io') > -1) {
                // leave it blank because i dunno, it makes socket.io work.
                return;
            } else {
                socket.destroy();
            }
        })

        http.listen(options.port, () => {
            console.log('listening on *:' + options.port);
        })

        return {
            http: http,
            app: app,
            io: io,
            videoSocket: wss,
        }
    },
    getIpAddress: (req) => {
        return req.headers['cf-connecting-ip'] ||
            req.headers["CF-Connecting-IP"] ||
            req.headers["'x-forwarded-for"] ||
            req.connection.remoteAddress
    }
}
