#!/usr/bin/env bash

# use lsusb to get your usb path
sudo ./usbreset /dev/bus/usb/001/008

# USB 3.1 Only
for port in $(lspci | grep xHCI | cut -d' ' -f1); do
    echo -n "0000:${port}"| sudo tee /sys/bus/pci/drivers/xhci_hcd/unbind;
    sleep 5;
    echo -n "0000:${port}" | sudo tee /sys/bus/pci/drivers/xhci_hcd/bind;
    sleep 5;
done

# All USB
for port in $(lspci | grep USB | cut -d' ' -f1); do
    echo -n "0000:${port}"| sudo tee /sys/bus/pci/drivers/xhci_hcd/unbind;
    sleep 5;
    echo -n "0000:${port}" | sudo tee /sys/bus/pci/drivers/xhci_hcd/bind;
    sleep 5;
done
