//plus, down, l_stick, b, y, zr, capture, up, r, home, x, right, l, r_stick, a, zl, minus, left
var selectedController = 0;
var keyLegend = {
    "0": "b",
    "1": "a",
    "2": "y",
    "3": "x",
    "4": "l",
    "5": "r",
    "6": "zl",
    "7": "zr",
    "8": "minus",
    "9": "plus",
    "10": "l_stick",
    "11": "r_stick",
    "12": "up",
    "13": "down",
    "14": "left",
    "15": "right",
}
var lastState = {
    sticks: {
        left: {},
        right: {},
    }
}
var hasGP = false;
var repGP;
var stickBase = 2047
var deadZoneThreshold = 0.14
var outerDeadZone = 0.75
if(kbdefault === 'ghost'){
    outerDeadZone = 1
}
// function openFullscreen(elem) {
//   if (elem.requestFullscreen) {
//     elem.requestFullscreen();
//   } else if (elem.webkitRequestFullscreen) { /* Safari */
//     elem.webkitRequestFullscreen();
//   } else if (elem.msRequestFullscreen) { /* IE11 */
//     elem.msRequestFullscreen();
//   }
// }

function canGame() {
   return "getGamepads" in navigator;
}

function convertStickAxisTo2048(value){
   var newVal = parseInt(value * stickBase) + stickBase
   return newVal
}

function convertStickReverseAxisTo2048(value){
   var newVal = parseInt(-value * stickBase) + stickBase
   return newVal
}
//
function convertAxis3toDPad(gp,commandsToSend){
    var horizontal = gp.axes[4]
    if(horizontal){
        // var vertical = gp.axes[5]
        // var vKey = vertical > 0 ? 'up' : vertical < 0 ? 'down' : null
        // if(
        //     vKey && lastState[vKey] !== vertical
        // ){
        //     lastState[vKey] = vertical
        //     commandsToSend.push(`hold ${vKey}`)
        // }else{
        //     commandsToSend.push(`release ${vKey}`)
        // }
        // var hKey = horizontal > 0 ? 'right' : horizontal < 0 ? 'left' : null
        // if(
        //     lastState[hKey] && lastState[hKey] !== hKey
        // ){
        //     if(hKey){
        //         commandsToSend.push(`hold ${hKey}`)
        //     }else{
        //         commandsToSend.push(`release ${lastState[hKey]}`)
        //     }
        //     lastState[hKey] = hKey
        // }
    }
}

function getStickValues(gp,commandsToSend){
    for(var i = 0; i < 4; i += 2) {
        var label = i === 0 ? 'left' : 'right'
        var horizontal = gp.axes[i] * outerDeadZone
        var vertical = gp.axes[i + 1] * outerDeadZone
        var newH = convertStickAxisTo2048(horizontal > deadZoneThreshold || horizontal < -deadZoneThreshold ? horizontal : 0)
        var newV = convertStickReverseAxisTo2048(vertical > deadZoneThreshold || vertical < -deadZoneThreshold ? vertical : 0)
        if(
            newH !== lastState.sticks[label].h
        ){
            commandsToSend.push(`stick ${label} h ${newH}`)
        }
        if(
            newV !== lastState.sticks[label].v
        ){
            commandsToSend.push(`stick ${label} v ${newV}`)
        }
        lastState.sticks[label].h = newH
        lastState.sticks[label].v = newV
    }
}

function getButtonValues(gp,commandsToSend){
    $.each(keyLegend,function(code,key){
        if(gp.buttons[code] && gp.buttons[code].pressed){
            if(!lastState[key])commandsToSend.push(`hold ${key}`)
            lastState[key] = true
        }else{
            if(lastState[key])commandsToSend.push(`release ${key}`)
            lastState[key] = false
        }
    })
}

function sendCommandsToSwitch(commandsToSend){
    if(commandsToSend.length > 0){
        controllerAction(commandsToSend)
    }
}

function reportOnProperGamepad() {
    try{
        var gp = navigator.getGamepads()[0];
        var commandsToSend = []
        getButtonValues(gp,commandsToSend)
        getStickValues(gp,commandsToSend)
        sendCommandsToSwitch(commandsToSend)
    }catch(err){
        err.stack.split('\n').forEach((text) => {
            writeLog(text)
        })
        stopReporting()
    }
}

function reportOnOddballGamepad() {
    try{
        var gp = navigator.getGamepads()[0];
        var commandsToSend = []
        getButtonValues(gp,commandsToSend)
        getStickValues(gp,commandsToSend)
        convertAxis3toDPad(gp,commandsToSend)
        sendCommandsToSwitch(commandsToSend)
    }catch(err){
        err.stack.split('\n').forEach((text) => {
            writeLog(text)
        })
        stopReporting()
    }
}
var reportOnGamepad = reportOnProperGamepad
function startReporting(){
    if(hasGP){
        var gp = navigator.getGamepads()[0];
        if(Object.keys(gp.buttons).length < 10){
            writeLog(`Odd GamePad Detected. Some buttons may not work.`)
            // stickBase = 2048
            convertStickReverseAxisTo2048 = convertStickAxisTo2048
            reportOnGamepad = reportOnOddballGamepad
        }
        repGP = window.setInterval(reportOnGamepad,25);
    }
}

function stopReporting(){
    window.clearInterval(repGP)
}

function loadAmiibo(amiibo){
    controllerAction([`nfc /home/nswitch/web-nintendo-switch-controller/amiibo/${amiibo}.bin`])
    controllerAction([`hold up`])
    controllerAction([`stick r h 4096`])
    setTimeout(function(){
        controllerAction([`release up`])
        controllerAction([`hold l`])
        controllerAction([`release l`])
    },1000)
}

$(document).ready(function() {
   if(canGame()) {
       var prompt = "Connect your GamePad to start playing!";
       writePrettyLog([
           `<b class="text-primary">${prompt}</b>`,
       ])
       $(window).on("gamepadconnected", function() {
           hasGP = true;
           writePrettyLog([
               `<b class="text-success">Gamepad connected!</b>`,
           ])
           startReporting();
       });

       $(window).on("gamepaddisconnected", function() {
           writePrettyLog([
               `<b class="text-primary">${prompt}</b>`,
           ])
           stopReporting()
       });

       $(window).focus(function() {
           startReporting()
       }).blur(function() {
           stopReporting()
       });
   }else{
       writeLog(`It appears this client device cannot use GamePads :(`,'danger')
   }
});
