var loadedPlayers = {}
$(document).ready(function(){
    var statsViewers = document.getElementById("statsViewers")
    var pingTime = document.getElementById("pingTime")
    window.lastPlayers = {}
    window.controllerId = getUUID()
    var webSocket = io({
        transports: ['websocket'],
        query: `uuid=${getUUID()}`
    });
    function controllerAction(theCommand){
        webSocket.emit('com',theCommand)
    }
    webSocket.on('controllerId',function(controllerId){
        window.controllerId = controllerId
    })
    webSocket.on('initiate',function(data){
        window.controllerId = webSocket.id
        clearPlayerList()
        $.each(data.users,function(n,user){
            drawUserRow(user)
            updateUserRow(user)
            loadedPlayers[user.id] = user
        })
        lastPlayers = data.currentPlayers
        if(data.amiibos){
            var html = ``
            $.each(data.amiibos,function(n,v){
                html += `<a class="dropdown-item" nfc="${v}">${v}</a>`
            })
            $(`[aria-labelledby="dropdownMenuLink"]`).html(html)
        }
        loadNickame()
    })
    webSocket.on('out',function(data){
        console.log(data)
    })
    webSocket.on('err',function(data){
        console.log(data)
    })
    webSocket.on('viewers',function(viewers){
        statsViewers.innerHTML = `${viewers}`
        console.log(`Current Number of Viewers : ${viewers}`)
    })
    webSocket.on('heartbeat',function(viewers){
        webSocket.emit('heartbeat',{time: new Date()})
    })
    webSocket.on('ms',function(ms){
        pingTime.innerHTML = `${ms}`
    })
    webSocket.on('lockCom',function(lockedBy){
        writeLog(`Controller is currently in use by ${lockedBy}`)
    })
    webSocket.on('vote',function(data){
        function makeBase(word){
            return `${data.userId} (${data.currentlyVoted.length} / ${data.maxRequired}) voted to <b>${word}</b> the controller.`
        }
        function isReason(thing){
            return data.reason.indexOf(thing)
        }
        switch(true){
            case isReason('controllerReset'):
                writeLog(`${makeBase(`reset`)} Only do this if it isn't working.`)
            break;
            case isReason('controllerUnbind'):
                writeLog(`${makeBase(`unbind`)} Only do this if the current player isn't sharing.`)
            break;
        }
    })
    webSocket.on('userConnected',(user) => {
        drawUserRow(user)
        updateUserRow(user)
        loadedPlayers[user.id] = user
    })
    webSocket.on('userUpdate',(user) => {
        updateUserRow(user)
    })
    webSocket.on('userDisconnected',(userId) => {
        removeUserRow(userId)
    })
    webSocket.on('chatMessage',(msg) => {
        var userId = msg.senderId
        writeLog(`<b><small class="text-muted">${userId}</small></b><br><span style="font-size:80%;color:#fff">${msg.text}</span><br><small class="text-muted">${msg.time}</small>`,' clear-logs')
    })
    webSocket.on('controllerStdout',(text) => {
        if(text.indexOf('cmd >>') === -1)writeLog(text)
    })
    webSocket.on('controllerStderr',(text) => {
        writeLog(text)
    })
    webSocket.on('controllerChange',(controllerNumber) => {
        selectedController = parseInt(controllerNumber)
        writePrettyLog([
            `<b class="text-info">You are now paired to Controller #${selectedController + 1}</b>`,
        ])
    })
    webSocket.on('playerChange',(data) => {
        var userId = data.userId
        var controllerNumber = data.controllerNumber
        var controllerLabel = `<small class="text-warning">Controller #${parseInt(controllerNumber) + 1}</small>`
        var name = userId
        try{
            name = loadedPlayers[userId].uuid
        }catch(err){
        }
        if(controllerNumber === selectedController && userId === controllerId){
            writePrettyLog([
                controllerLabel,
                `<b class="text-danger">You are now in control.</b>`
            ])
            lastPlayers[controllerNumber] = `${userId}`
        }else if(userId){
            setUserRowIcon(userId,`gamepad`)
            writePrettyLog([
                controllerLabel,
                `<b><small class="text-warning">Controller is now in control by ${name}</small></b>`,
                `<span style="font-size:80%;" class="text-muted">You can vote to change players or ask the player to give you control.</span>`
            ])
            lastPlayers[controllerNumber] = `${userId}`
        }else{
            writePrettyLog([
                controllerLabel,
                `<b class="text-success">No one is playing! Connect your controller and press a button!</b>`,
            ])
            if(lastPlayers[controllerNumber])setUserRowIcon(lastPlayers[controllerNumber],`circle`)
            lastPlayers[controllerNumber] = null
        }
    })
    window.controllerAction = controllerAction
    window.sendChatMessage = (message) => {
        webSocket.emit('chatMessage',message)
    }
    window.sendData = (type,message) => {
        webSocket.emit(type,message)
    }
})
