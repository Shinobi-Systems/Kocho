var showLogWindow = false;
$(document).ready(function(){
    var logElement = $('#logs')
    var chatBarInput = $('#chatBarInput')
    var playerList = $('#playerList')
    var logNumber = 0
    function uuidv4() {
      return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
      });
    }
    window.getUUID = function(){
        var myNameIs = getQueryString().mynameis
        var uuid = localStorage.getItem('shinobi-games-uuid')
        if(!uuid){
            uuid = uuidv4()
            localStorage.setItem('shinobi-games-uuid',`${uuid}`)
        }
        return uuid
    }
    window.getQueryString = function(){
        var newObject = {}
        var string = location.search
        if(string && string !== '?'){
            $.each(string.replace('?','').split('&'),(n,part) => {
                console.log(n,part)
                var parts = part.split('=')
                var key = parts[0]
                var value = parts[1]
                newObject[key] = value
            })
        }
        return newObject
    }
    window.getWebSocketAddress = function(suffix){
        console.log(location)
        let webSocketServerAddress = location.origin.replace('http://','ws://').replace('https://','wss://').split(':')
        webSocketServerAddress = webSocketServerAddress[0] + ':' + webSocketServerAddress[1]  + ':' + (location.port ? location.port : location.protocol === 'https:' ? '443' : '80') + '/' + (suffix || '')
        return webSocketServerAddress
    }
    window.writeLog = function(log,classes,attributes){
        var divId = `log_` + logNumber
        logElement.append(`<div id="${divId}"><pre class="btn log-row btn-block btn-sm btn-${classes || 'default clear-logs'} text-left" ${attributes || ''}>${log}</pre></div>`)
        setTimeout(() => {
            $(`#${divId}`).remove()
        },30000)
        ++logNumber
        logElement[0].scrollTop = logElement[0].scrollHeight;
    }
    window.writePrettyLog = function(arrayOfHtml,time,addedClasses,attributes){
        time = time ? time : new Date()
        writeLog(`${arrayOfHtml.join('<br>')}<br><small class="text-muted">${new Date()}</small>`,addedClasses || ' clear-logs',attributes)
    }
    window.setUserRowIcon = function(userId,icon){
        var iconElement = $(`#user_${userId}`).find('i')
        iconElement[0].className = '';
        iconElement.addClass(`fa fa-${icon} text-${icon === 'gamepad' ? 'warning' : 'primary'}`)
    }
    window.drawUserRow = function(user){
        if($(`#user_${user.id}`).length === 0){
            playerList.append(`<div id="user_${user.id}" user-id="${user.id}" class="user-row text-right"><a title="Give Control to this Person" class="give-control float-left mr-1"><i class=""></i></a> <span class="name"></span></div>`)
        }
    }
    window.updateUserRow = function(user){
        var userRow = $(`#user_${user.id}`)
        setUserRowIcon(user.id,user.isPlaying ? 'gamepad' : 'circle')
        userRow.find(`.name`).text(user.name || user.uuid || user.id)
        userRow.attr('title',`${user.uuid ? 'UUID : ' + user.uuid + ', ' : ''}Signed on at ${user.joinTime || new Date()}`)
    }
    window.removeUserRow = function(userId){
        $(`#user_${userId}`).remove()
    }
    window.clearPlayerList = function(userId){
        playerList.empty()
    }
    $('body')
    .on('click','[select-controller]',function(){
        var controllerNumber = parseInt($(this).attr('select-controller')) || 0
        selectedController = controllerNumber
        sendData('chooseController',controllerNumber)
    })
    .on('click','.clear-logs',function(){
        logElement.empty()
    })
    .on('click','.give-control',function(){
        var userIdToGiveTo = $(this).parents('.user-row').attr('user-id')
        sendData(`giveControl`,{
            userId: userIdToGiveTo,
            controllerNumber: selectedController,
        })
    })
    .on('click','.remove-my-control',function(){
        sendData(`giveControl`,null)
    })
    $('.vote-controller-reset').click(function(){
        sendData(`voteControllerReset`)
    })
    $('.vote-controller-unbind').click(function(){
        if(lastPlayers[0] === controllerId){
            sendData(`giveControl`,null)
        }else{
            sendData(`voteControllerUnbind`)
        }
    })
    $('.can-i-play').click(function(){
        if(lastPlayers[0] === controllerId){
            writeLog(`You're already playing.`,'btn btn-success')
        }else if(!lastPlayers[0]){
            writeLog(`<b>No one is playing! Connect your controller and press a button!</b>`,'btn btn-success')
        }else{
            sendChatMessage(`Can i play? :)`)
        }
    })
    $('.focus-chat').click(function(){
        $('#chatBarInput').focus()
    })
    $('.fps-throttle').click(function(){
        videoSocket.send('throttle')
    })
    $('body').on('click','[nfc]',function(){
        var nfc = $(this).attr('nfc')
        loadAmiibo(nfc.replace('.bin',''))
    })
    $('#chatBarInput').focus(function(){
        showLogWindow = true;
        logElement.addClass('force-show')
    }).blur(function(){
        showLogWindow = false;
        logElement.removeClass('force-show')
    })
    $('#chatBar').submit(function(e){
        e.preventDefault()
        var message = chatBarInput.val().trim()
        chatBarInput.val('')
        sendChatMessage(message)
        return false;
    })
    setTimeout(function(){
        setTimeout(function(){
            writePrettyLog([
                `<b class="text-warning">Click to see Keyboard Mapper</b>`,
            ],null, ' ', `data-toggle="modal" data-target="#keyMapper"`)
            writePrettyLog([
                `<b class="text-warning">Please do not change the game or use other accounts. Let's enjoy together.</b>`,
            ])
            setTimeout(function(){
                writePrettyLog([
                    `<b class="text-danger">Join the Shinobi Games Community Chat on Discord!</b>`,
                ],null,' ',`onclick="window.location.href='https://discord.gg/f7Ur2VcZhf'"`)
                writePrettyLog([
                    `<b class="text-warning">Click to Change your Name</b>`,
                ],null, ' ', `data-toggle="modal" data-target="#userSettings"`)
            },3000)
        },3000)
    },3000)
})
