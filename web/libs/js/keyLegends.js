function getDefaultKeyboardStickLegend(){
    return {
        "40": ["right v","0"], //Down
        "37": ["right h","0"], //Left
        "39": ["right h","4094"], //Right
        "38": ["right v","4094"], //Up
        "101": ["right v","0"], //Down
        "100": ["right h","0"], //Left
        "102": ["right h","4094"], //Right
        "104": ["right v","4094"], //Up
        "75": ["right v","0"], //Down
        "74": ["right h","0"], //Left
        "76": ["right h","4094"], //Right
        "73": ["right v","4094"], //Up
        "87": ["left v","4094"], //W
        "65": ["left h","0"], //A
        "83": ["left v","0"], //S
        "68": ["left h","4094"], //D
    }
}
function getDefaultKeyboardButtonLegend(){
    return {
        "16": "b",
        "105": "a",
        "103": "y",
        "79": "a",
        "85": "y",
        "32": "x",
        "81": "l",
        "69": "r",
        "90": "zl",
        "88": "zr",
        "222": "minus",
        "13": "plus",
        "17": "l_stick",
        "82": "r_stick",
        "51": "up",
        "52": "down",
        "49": "left",
        "50": "right",
        // "220": "home",
    }
}
function getKeyboardStickLegend(mapName){
    switch(mapName){
        case'mkart8':
            return {
                "40": ["left v","0"], //Down
                "37": ["left h","0"], //Left
                "39": ["left h","4094"], //Right
                "38": ["left v","4094"], //Up
                "101": ["left v","0"], //Down
                "100": ["left h","0"], //Left
                "102": ["left h","4094"], //Right
                "104": ["left v","4094"], //Up
                "75": ["left v","0"], //Down
                "74": ["left h","0"], //Left
                "76": ["left h","4094"], //Right
                "73": ["left v","4094"], //Up
            }
        break;
        case'ghost':
            return {
                "40": ["right v","0"], //Down
                "37": ["right h","0"], //Left
                "39": ["right h","4094"], //Right
                "38": ["right v","4094"], //Up
                "101": ["right v","0"], //Down
                "100": ["right h","0"], //Left
                "102": ["right h","4094"], //Right
                "104": ["right v","4094"], //Up
                "75": ["right v","0"], //Down
                "74": ["right h","0"], //Left
                "76": ["right h","4094"], //Right
                "73": ["right v","4094"], //Up
                "87": ["left v","4094"], //W
                "65": ["left h","0"], //A
                "83": ["left v","0"], //S
                "68": ["left h","4094"], //D
            }
        break;
        default:
            return getDefaultKeyboardStickLegend()
        break;
    }
}
function getKeyboardButtonLegend(mapName){
    switch(mapName){
        case'mkart8':
            return {
                "66": "b",
                "16": "a",
                "89": "y",
                "88": "x",
                "32": "l",
                "17": "r",
                "68": "r",
                "222": "minus",
                "13": "plus",
                "81": "l_stick",
                "69": "r_stick",
                "51": "up",
                "52": "down",
                "49": "left",
                "50": "right",
                // "220": "home",
            }
        break;
        case'ghost':
            return {
                "32": "b",
                "105": "a",
                "103": "y",
                "57": "a",
                "56": "a",
                "85": "y",
                "79": "x",
                "81": "l",
                "69": "r",
                "90": "zl",
                "88": "zr",
                "222": "minus",
                "13": "plus",
                "16": "l_stick",
                "17": "r_stick",
                "51": "up",
                "52": "down",
                "49": "left",
                "50": "right",
            }
        break;
        default:
            return getDefaultKeyboardButtonLegend()
        break;
    }
}
var humanActionNames = {
    "left v:4094" : "Left Stick Up",
    "left h:0" : "Left Stick Left",
    "left v:0" : "Left Stick Down",
    "left h:4094" : "Left Stick Right",
    "right v:4094" : "Right Stick Up",
    "right h:0" : "Right Stick Left",
    "right v:0" : "Right Stick Down",
    "right h:4094" : "Right Stick Right",
    "l" : "L (L1)",
    "r" : "R (R1)",
    "zl" : "ZL (L2)",
    "zr" : "ZR (R2)",
    "l_stick" : "Left Stick Click (L3)",
    "r_stick" : "Right Stick Click (R3)",
    "left" : "D-Pad Left",
    "right" : "D-Pad Right",
    "up" : "D-Pad Up",
    "down" : "D-Pad down",
    "y" : "Y (Square)",
    "a" : "A (Circle)",
    "b" : "B (Cross)",
    "x" : "X (Triangle)",
    "plus" : "Plus (Options, Start)",
    "minus" : "Minus (Share, Select)",
}
var stickLegend = getDefaultKeyboardStickLegend()
var keyboardKeyLegned = getDefaultKeyboardButtonLegend()
var humanizedKeyLegend = {
    "W" : "Left Stick Up",
    "A" : "Left Stick Left",
    "S" : "Left Stick Down",
    "D" : "Left Stick Right",
    "I" : "Right Stick Up",
    "J" : "Right Stick Left",
    "K" : "Right Stick Down",
    "L" : "Right Stick Right",
    "Q" : "L (L1)",
    "E" : "R (R1)",
    "Z" : "ZL (L2)",
    "X" : "ZR (R2)",
    "Ctrl" : "Left Stick Click (L3)",
    "R" : "Right Stick Click (R3)",
    "1" : "D-Pad Left",
    "2" : "D-Pad Right",
    "3" : "D-Pad Up",
    "4" : "D-Pad down",
    "7 (Numpad)" : "Y (Square)",
    "9 (Numpad)" : "A (Circle)",
    "U" : "Y (Triangle)",
    "O" : "A",
    "Shift" : "B",
    "Spacebar" : "X",
    "|" : "Home",
    "Enter (Return)" : "Plus",
    "'" : "Minus",
}
