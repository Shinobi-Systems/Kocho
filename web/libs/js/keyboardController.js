var keyMapper = $('#keyMapper')
var keyMapperList = $('#keyMapperList')
var chatBarInput = $('#chatBarInput')
var wasPressed = {}
var keysPressed = {}
var sticksMoved = {}
function sendKeysPressed(){
    var commandsToSend = []
    $.each(keyboardKeyLegned,function(code,key){
        if(keysPressed[code]){
            if(!wasPressed[code]){
                wasPressed[code] = true
                commandsToSend.push(`hold ${key}`)
            }
        }else{
            if(wasPressed[code]){
                commandsToSend.push(`release ${key}`)
                wasPressed[code] = false
            }
        }
    })
    $.each(sticksMoved,function(keyCode,isTrue){
        var stickAction = stickLegend[keyCode]
        if(isTrue){
            if(!wasPressed[keyCode]){
                wasPressed[keyCode] = true
                commandsToSend.push(`stick ${stickAction[0]} ${stickAction[1]}`)
            }
        }else{
            if(wasPressed[keyCode]){
                wasPressed[keyCode] = false
                commandsToSend.push(`stick ${stickAction[0]} 2048`)
            }
        }
    })
    console.log(`commandsToSend`)
    console.log(commandsToSend)
    controllerAction(commandsToSend)
}
function saveCustomKeyMap(newKeys){
    localStorage.setItem('shinobi-keyboard-map-' + consoleId,JSON.stringify(newKeys))
}
function getCustomKeyMap(){
    var inStorage = {
        sticks: getKeyboardStickLegend(kbdefault),
        buttons: getKeyboardButtonLegend(kbdefault),
    }
    try{
        inStorage = JSON.parse(localStorage.getItem('shinobi-keyboard-map-' + consoleId)) || inStorage
    }catch(err){

    }
    return inStorage
}
function splitStickValues(stickSet){
    var newStickSet = {}
    $.each(stickSet,function(key,value){
        newStickSet[key] = typeof value === 'string' ? value.split(':') : value
    })
    return newStickSet
}
function loadCustomKeyMap(newKeys){
    var defaultKeys = {
        sticks: getKeyboardStickLegend(kbdefault),
        buttons: getKeyboardButtonLegend(kbdefault),
    }
    var inStorage = getCustomKeyMap()
    if(inStorage){
        stickLegend = splitStickValues(inStorage.sticks)
        keyboardKeyLegned = inStorage.buttons
    }else{
        stickLegend = splitStickValues(defaultKeys.sticks)
        keyboardKeyLegned = defaultKeys.buttons
    }
}
function getMappableKeys(){
    var mappables = {}
    var customKeys = {
        sticks: getDefaultKeyboardStickLegend(),
        buttons: getDefaultKeyboardButtonLegend(),
    }
    $.each(customKeys.buttons,(n,value) => {
        mappables[value] = true
    })
    $.each(customKeys.sticks,(n,value) => {
        mappables[value.join(':')] = true
    })
    return Object.keys(mappables)
}
function buildMappableKeyOptions(selectedValue){
    var theKeys = getMappableKeys()
    var html = ''
    $.each(theKeys,function(n,value){
        html += `<option value="${value}" ${selectedValue === value ? 'selected' : ''}>${humanActionNames[value] || value}</option>`
    })
    return html
}
function getHumanStringFromKeyCode(keyCode){
    var name = ''
    var parsedCode = parseInt(keyCode)
    console.log(parsedCode)
    if(!keyCode){
        return 'None Selected'
    }
    switch(parsedCode){
        case 9:
            name = 'Tab'
        break;
        case 20:
            name = 'Caps Lock'
        break;
        case 13:
            name = 'Enter'
        break;
        case 222:
            name = "Single Quote"
        break;
        case 32:
            name = "Spacebar"
        break;
        case 16:
            name = "Shift"
        break;
        case 17:
            name = "Ctrl"
        break;
        case 37:
            name = "Left Arrow"
        break;
        case 38:
            name = "Up Arrow"
        break;
        case 39:
            name = "Right Arrow"
        break;
        case 40:
            name = "Down Arrow"
        break;
        case 96:
        case 97:
        case 98:
        case 99:
        case 100:
        case 101:
        case 102:
        case 103:
        case 104:
        case 105:
            name = `${parsedCode - 96} Numpad`
        break;
        default:
            name = String.fromCharCode(keyCode)
        break;
    }
    return name
}
function drawKeyMapperRow(key,value,addToTop){
    var optionValue = typeof value === 'string' ? value : value.join(':')
    keyMapperList[addToTop ? 'prepend' : 'append'](`<div class="key-map-row">
        <div class="d-flex key-action-pair">
            <div style="flex: 2" class="pr-1">
                <div class="form-group">
                    <input placeholder="Key" class="form-control form-control-sm key-selected" value="${key}" />
                </div>
            </div>
            <div style="flex: 4" class="pr-1">
                <div class="form-group">
                    <input disabled class="form-control form-control-sm key-selected-human" value="${getHumanStringFromKeyCode(key)}" />
                </div>
            </div>
            <div style="flex: 4" class="pr-1">
                <div class="form-group">
                    <select class="form-control form-control-sm action-selected">${buildMappableKeyOptions(optionValue)}</select>
                </div>
            </div>
            <div style="flex: 1">
                <a class="delete-row btn btn-default"><i class="fa fa-times"></i></a>
            </div>
        </div>
    </div>`)
}
function drawKeyActionPairs(keys){
    keyMapperList.empty()
    $.each(keys,function(type,bindings){
        $.each(bindings,drawKeyMapperRow)
    })
}
function drawMappableKeyOptions(){
    var customKeys = getCustomKeyMap()
    drawKeyActionPairs(customKeys)
}
function getSetKeyValues(){
    var newMappings = {
        sticks: {},
        buttons: {},
    }
    keyMapperList.find('.key-action-pair').each(function(n,v){
        var el = $(v)
        var key = el.find('.key-selected').val().trim()
        var value = el.find('.action-selected').val().trim()
        var isStick = false;
        if(value.indexOf(':') > -1){
            isStick = true
        }
        if(key && value)newMappings[isStick ? 'sticks' : 'buttons'][key] = value;
    })
    return newMappings
}
function gamePlayKeydownBind(event) {
  if (event.defaultPrevented) {
    return; // Do nothing if the event was already processed
  }
  var commandsToSend = []
  var keyCode = event.keyCode
  if(stickLegend[keyCode]){
      if(sticksMoved[keyCode])return;
      sticksMoved[keyCode] = true
  }else if(keyboardKeyLegned[keyCode]){
      if(!showLogWindow){
          if(keysPressed[keyCode])return;
          keysPressed[keyCode] = true
      }
  }else{
      // Quit when this doesn't handle the key event.
      if(keyCode === 27 || keyCode === 192){
          if(showLogWindow){
              chatBarInput.blur()
          }else{
              chatBarInput.focus()
          }
          $('#videoStream').unbind("mousemove").unbind("mousedown").unbind("mouseup")
      }
      return;
  }
  if(!showLogWindow){
      sendKeysPressed()
      event.preventDefault();
  }
}
function gamePlayKeyupBind(event) {
  if (event.defaultPrevented) {
    return; // Do nothing if the event was already processed
  }
  var commandsToSend = []
  var keyCode = event.keyCode
  if(stickLegend[keyCode]){
      if(!sticksMoved[keyCode])return;
      sticksMoved[keyCode] = false;
  }else if(keyboardKeyLegned[keyCode]){
      if(!showLogWindow){
          if(!keysPressed[keyCode])return;
          keysPressed[keyCode] = false
      }
  }else{
      // Quit when this doesn't handle the key event.
      return;
  }
  if(!showLogWindow){
      sendKeysPressed()
      event.preventDefault();
  }
}
function keyMapperKeydownBind(event){
    var el = $(this)
    var keyCode = event.keyCode
    selectedKeyMapperInput.find('.key-selected').val(keyCode)
    selectedKeyMapperInput.find('.key-selected-human').val(getHumanStringFromKeyCode(keyCode))
}
var selectedKeyMapperInput = null
var keydownAction = gamePlayKeydownBind
var keyupAction = gamePlayKeyupBind
$(document).ready(function() {
    window.addEventListener("keydown", function(event){
        keydownAction(event)
    }, true);
    window.addEventListener("keyup", function(event){
        keyupAction(event)
    }, true);
    $('.show-keyboard-controls').click(function(){
        writeLog(JSON.stringify(humanizedKeyLegend,null,3))
    })
    $('#addNewKeyMap').click(function(){
        drawKeyMapperRow('','',true)
    })
    $('#saveCustomMapSet,.saveCustomMapSet').click(function(){
        var newVals = getSetKeyValues()
        saveCustomKeyMap(newVals)
        loadCustomKeyMap()
    })
    $('#setAsDefault').click(function(){
        drawKeyActionPairs({
            sticks: getKeyboardStickLegend(kbdefault),
            buttons: getKeyboardButtonLegend(kbdefault),
        })
    })
    keyMapper.on('shown.bs.modal',function(){
        showLogWindow = true
        drawMappableKeyOptions()
    })
    .on('hidden.bs.modal',function(){
        showLogWindow = false
    })
    .on('keydown','.key-selected',function(e){
        e.preventDefault()
    })
    .on('focus','.key-selected',function(){
        selectedKeyMapperInput = $(this).parents('.key-action-pair')
        keydownAction = keyMapperKeydownBind
        keyupAction = function(){}
        console.log('Mapping')
    })
    .on('blur','.key-selected',function(){
        selectedKeyMapperInput = null
        keydownAction = gamePlayKeydownBind
        keyupAction = gamePlayKeyupBind
    })
    .on('click','.select-key-map',function(){
        var mapName = $(this).attr('select-key-map')
        console.log({
            sticks: getKeyboardStickLegend(mapName),
            buttons: getKeyboardButtonLegend(mapName),
        },mapName)
        drawKeyActionPairs({
            sticks: getKeyboardStickLegend(mapName),
            buttons: getKeyboardButtonLegend(mapName),
        })
    })
    keyMapperList.on('click','.delete-row',function(){
        $(this).parents('.key-action-pair').remove()
    })
    loadCustomKeyMap()
})


// Mouse Joystick

var deadZone = 5;
var previousX = 0;
var previousY = 0;
var mouseCommandsToSend = {};
var mouseCommandsSentLast = {};
var stopTimer = false;
function resetMouseCommandsToSend(center){
    mouseCommandsToSend = {
        "stick right h 4094": false, //right
        "stick right h 0": false, //left
        "stick right v 4094": false, //up
        "stick right v 0": false, //down
        "stick right h 2048": true, //center horiztonal
        "stick right v 2048": true, //center vertical
    }
}
function lockMouse(){
    var element = $("#videoStream")[0]
    setTimeout(function(){
        element.requestPointerLock()
        setTimeout(function(){
            console.log(document.pointerLockElement)
        },3000)
    },3000)
}
function unlockMouse(element){
    var requestPointerRelease = document.exitPointerLock ||
			   document.mozExitPointerLock ||
			   document.webkitExitPointerLock;
    requestPointerRelease();
}
function getMouseMovement(){
    var commandList = []
    $.each(mouseCommandsToSend,function(key,isSending){
        if(isSending)commandList.push(key)
    })
    return [commandList.join(' && ')];
}
function reportMouseData(e){
    var cordX, cordY;
    if(document.pointerLockElement) {
        cordX = e.screenX
        cordY = e.screenX
    } else {
        cordX = e.pageX
        cordY = e.pageY
    }
    console.log(e)
    if (cordX > previousX + deadZone) {
        //right
        console.log('right')
        mouseCommandsToSend["stick right h 4094"] = true;
        mouseCommandsToSend["stick right h 2048"] = false;
    } else if (cordX < previousX - deadZone) {
        //left
        console.log('left')
        mouseCommandsToSend["stick right h 0"] = true;
        mouseCommandsToSend["stick right h 2048"] = false;
    }
    if (cordY < previousY - deadZone) {
        //up
        console.log('up')
        mouseCommandsToSend["stick right v 4094"] = true;
        mouseCommandsToSend["stick right v 2048"] = false;
    } else if (cordY > previousY + deadZone) {
        //down
        console.log('down')
        mouseCommandsToSend["stick right v 0"] = true;
        mouseCommandsToSend["stick right v 2048"] = false;
    }
    if(Object.values(mouseCommandsSentLast).join(',') === Object.values(mouseCommandsToSend).join(','))return;
    mouseCommandsSentLast = Object.assign(mouseCommandsToSend,{})
    clearTimeout(stopTimer);
    stopTimer = setTimeout(function () {
        resetMouseCommandsToSend(true)
        console.log('center')
        controllerAction(['stick right h 2048', 'stick right v 2048'])
    }, 150);
    controllerAction(getMouseMovement())
    console.log(getMouseMovement())
    previousX = cordX;
    previousY = cordY;
}
function bindMouseToRightStick(opts) {
    // lockMouse();
    var i = 0;
    $('#videoStream').bind("mousemove", function (e) {
        var activeElement = e.target || e.srcElement;
        resetMouseCommandsToSend()
        if(i == 5){
            reportMouseData(e)
            i = 0;
        }
        ++i
    })
    .bind('mousedown',function(e){
        e.preventDefault()
        if(e.which === '3'){
            controllerAction(['hold a'])
        }else{
            controllerAction(['hold y'])
        }
        return false;
    })
    .bind('mouseup',function(e){
        e.preventDefault()
        if(e.which === '3'){
            controllerAction(['release a'])
        }else{
            controllerAction(['release y'])
        }
        return false;
    })
    .dblclick(function(){
        bindMouseToRightStick()
    })
}
