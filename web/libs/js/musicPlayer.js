$(document).ready(function(){
    var loadedSongs = {}
    var musicPlayer = $('#musicPlayer')
    var musicPlayerList = $('#musicPlayerList')
    var audioElement = $('#musicPlayerAudioElement')
    var isPlaying = false;
    var nextSong = null;
    window.drawSongRow = function(file){
        musicPlayerList.append(`<div song-file="${file.webPath}" class="user-row text-right">
            <div>
                <a class="play-song">
                    <i class="fa fa-play"></i>
                </a>
            </div>
            <div class="song-info">
                <div><span class="name">${file.song}</span></div>
                <div><small class="artist">${file.artist}</small></div>
            </div>
        </div>`)
    }
    function getSongList(callback) {
        $.get(`/music/list`,function(data){
            callback(data)
        })
    }

    function startPlaying(url,timeIndex) {
        isPlaying = true
        var audioPlayer = audioElement[0]
        if(!audioPlayer.paused)audioPlayer.pause()
        audioPlayer.src = url
        audioPlayer.load()
        audioPlayer.currentTime = timeIndex
        audioPlayer.play()
        var rowEl = musicPlayerList.find(`[song-file="${url}"]`)
        var webPath = rowEl.attr('song-file')
        nextSong = rowEl.is(':last-child') ? musicPlayerList.find('[song-file]:first-child').attr('song-file') : rowEl.next().attr('song-file')
    }

    function stopPlaying(url,timeIndex) {
        isPlaying = false
        var audioPlayer = audioElement[0]
        if(!audioPlayer.paused)audioPlayer.pause()
    }
    audioElement[0].addEventListener('ended', function(){
        if(isPlaying){
            startPlaying(nextSong,0)
        }
    })

    getSongList(function(list){
        $.each(list,function(n,file){
            drawSongRow(file)
            loadedSongs[file.webPath] = file
        })
    })

    $('.toggle-music-player').click(function(){
        musicPlayer.toggleClass('closed')
    });

    $('body')
    .on('click','.play-song',function(){
        var rowEl = $(this).parents('[song-file]')
        var webPath = rowEl.attr('song-file')
        startPlaying(webPath,0)
    })
})
