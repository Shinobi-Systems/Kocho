var showLogWindow = false;
$(document).ready(function(){
    var theWindow = $('#userSettings')
    var theForm = theWindow.find('form')
    theForm.submit(function(e){
        e.preventDefault()
        var values = {}
        $.each(theForm.serializeArray(), function(i, field) {
            values[field.name] = field.value;
        })
        sendData('setName',values.mynameis)
        localStorage.setItem('shinobi-display-name',values.mynameis)
        // window.location.replace(location.href.replace('#','').split('?')[0] + '?mynameis=' + values.mynameis);
        return false;
    })
    theWindow.on('shown.bs.modal',function(){
        showLogWindow = true
    })
    .on('hidden.bs.modal',function(){
        showLogWindow = false
    })
    window.loadNickame = function(){
        var mynameis = getQueryString().mynameis || localStorage.getItem('shinobi-display-name')
        if(mynameis){
            theForm.find('input').val(mynameis)
            sendData('setName',mynameis)
        }
    }
})
