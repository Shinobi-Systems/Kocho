$(document).ready(function(){
    var currentWidth = $('#videoStream').width()
    var currentHeight = $('#videoStream').height()
    var ctx = document.getElementById("videoStream").getContext('2d');
    var socket;
    var buffer = [];
    var keepPlayingStreamOutOfFocus = false;
    var isAppleDevice = [
      'iPad Simulator',
      'iPhone Simulator',
      'iPod Simulator',
      'iPad',
      'iPhone',
      'iPod'
    ].includes(navigator.platform)
    // iPad on iOS 13 detection
    || (navigator.userAgent.includes("Mac") && "ontouchend" in document);
    function concatBuffers(myArrays){
        let length = 0;
        myArrays.forEach(item => {
          length += item.length;
        });
        let mergedArray = new Uint8Array(length);
        let offset = 0;
        myArrays.forEach(item => {
          mergedArray.set(item, offset);
          offset += item.length;
        });
        return mergedArray
    }
    function onBinaryMessage(blobBuffer) {
        var d = new Uint8Array(blobBuffer)
        buffer.push(d)
        if(d[d.length-2] === 0xFF && d[d.length-1] === 0xD9){
            var newBuffer = concatBuffers(buffer)
            buffer = []
            var img = new Image();
            var imgUrl = URL.createObjectURL(new Blob([newBuffer]));
            img.onload = function(){
                ctx.drawImage(img, 0, 0)
                URL.revokeObjectURL(imgUrl)
                delete(img)
            }
            img.src = imgUrl;
        }
    }
    var throttleFps = false;
    var fpsCount = 0
    var onMessageThrottled = function(event) {
        ++fpsCount
        if(fpsCount === 2){
            fpsCount = 0
            return
        }
        onBinaryMessage(event.data)
    };
    var onMessage = function(event) {
        onBinaryMessage(event.data)
    };
    var connectVideoStream = function(){
        let webSocketServerAddress = getWebSocketAddress('videoStream')
        if(socket && socket.close)socket.close(1000)
        socket = new WebSocket(webSocketServerAddress);
        // socket.binaryType = "arraybuffer";
        // socket.onmessage = onMessageThrottled
        socket.binaryType = "arraybuffer";
        socket.onmessage = onMessage

        socket.onopen = function() {
            setTimeout(function(){
                if(isAppleDevice){
                    // writeLog('Apple Device Detected, Throttling FPS','warning')
                    // videoSocket.send('throttle')
                }
            },500)
        };

        socket.onerror = function(error) {
          console.log(`[videoSocket] ${error.message}`);
        };

        socket.onclose = function(e) {
            if(e.code === 1000){
                socket = null
            }else{
                console.log('Socket is closed. Reconnect will be attempted in 1 second.', e.reason);
                setTimeout(function() {
                  connectVideoStream();
                }, 1000);
            }
        };
        window.videoSocket = socket
    };
    $('body').on('click','.throttleFps',function(){
        throttleFps = !throttleFps
        if(throttleFps){
            window.videoSocket.onmessage = onMessageThrottled
        }else{
            window.videoSocket.onmessage = onMessage
        }
    })
    .on('click','.play-stream-blurred',function(){
        keepPlayingStreamOutOfFocus = !keepPlayingStreamOutOfFocus
        $('.play-stream-blurred')
        .removeClass(keepPlayingStreamOutOfFocus ? 'badge-primary' : 'badge-warning')
        .addClass(!keepPlayingStreamOutOfFocus ? 'badge-primary' : 'badge-warning')
    })
    $(window).focus(function() {
        if(!keepPlayingStreamOutOfFocus || !socket)connectVideoStream()
    }).blur(function() {
        if(!keepPlayingStreamOutOfFocus)videoSocket.close(1000)
    });
    connectVideoStream()
})
